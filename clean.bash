#!/usr/bin/env bash

clear

tgt="$(pwd)/bin"
out="$(pwd)/out"

echo "### Ensure folders exist:"
mkdir -v -p $tgt;
mkdir -v -p $out;
echo

echo "### Clearing local binaries and profiling files:"
find $tgt -mindepth 1 -name "*" -delete -print
find $out -mindepth 1 -name "*" -delete -print
echo

echo "### Stack clean, remove YAML lock and local .stack-work:"
stack clean

find . -name "stack.yaml.lock" -delete -print
find . -name ".stack-work"     -type d -exec rm -rv "{}" \;
