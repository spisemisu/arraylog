#!/usr/bin/env bash

clear

bld="$(pwd)/.stack-work/install/x86_64-linux-nix/"
tgt="$(pwd)/bin"
out="$(pwd)/out"

echo "### Ensure folders exist:"
mkdir -v -p $bld;
mkdir -v -p $tgt;
mkdir -v -p $out;
echo

echo "### Clearing binaries and profiling files:"
find $bld -mindepth 1 -name "*" -delete -print
find $tgt -mindepth 1 -name "*" -delete -print
find $out -mindepth 1 -name "*" -delete -print
echo

echo "### Stack building:" 
stack build --profile
echo

src="$(find $bld -name 'bin')"

echo "### Copying binaries to local $tgt:" 
cp -v $src/* $tgt/
echo

echo "### Repoducible hashes:"
cd $tgt
for f in *; do
    # skip all non-binaries
    [[ $f == *.* ]] && continue
    echo -e $(sha256sum $f | cut -d " " -f 1): $f
done;
echo

echo "### Execute binaries (with profiling):"
for f in *; do
    # skip all non-binaries
    [[ $f == *.* ]] && continue
    ./$f +RTS -p -h -RTS
    mv $f.*   $out/
    hp2pretty $out/$f.hp
done;
echo
