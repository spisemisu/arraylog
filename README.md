Data.Array.Log256 (SAFE, idiomatic and ⊥)
=========================================

A SAFE and idiomatic (no corner cutting or strange `stuff` under the hood)
implementation of an immutable `array` in Haskell aiming for an idiomatic `low
memory footprint` by storing `chunks` of `256` elements in a `fixed vector`
(`data VF256 a = VF … , i00 :: a, … , iFF :: a }`) by sharing a single
constructor. A side-effect of this approach, is that we achieve `O(log₂₅₆ n)`
asymptotic time complexity for most operations.

Futhermore, in order to avoid the usage of the following pattern `data Present a
= Yes a | No` for when an element is present as it will add a constructor
(overhead) for each element. This is achieved with the usage of `⊥` (`bottom`)
combined with Haskells `lazy` nature as well as a `bitmap`, where each of the
`256` elements presence are stored as a single `bit`.

**Remark**: Please see the `References` below for [Simon Marlows][mb] answer
at `Stack Overflow` with regard of `memory footprint of Haskell data types`.

[mb]: https://simonmar.github.io/


## Basics

### Step-by-step insert example with an Array Log₈ with height equal to 0

```
We have an empty array A of size 64:

                                   □□□□□□□□

▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭

> Remark: ▭ denotes ⊥ (bottom).

The array A is updated with a value of type α on index A₃₁, where index < 64:

                                   □□□▣□□□□

▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭■  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭


The array A is updated with a value of type α on index A₂₉, where index < 64:

                                   □□□▣□□□□

▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭■▭■  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭

The array A is updated with a value of type α on index A₆₃, where index < 64:

                                   □□□▣□□□▣

▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭■▭■  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭▭  ▭▭▭▭▭▭▭■
```


### Step-by-step insert example with an Array Log₄ with height greater than 0

```
We want to insert a value of type α into index A₃₃ in an Array Log₄ of size 64

                                      □□□□

        □□□□                □□□□                □□□□                □□□□

▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭  ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭

> Remark: ▭ denotes ⊥ (bottom).

a) Update top bitmap index (node):

   33 .>. 4     = 2
    2 .&. (4-1) = 2

                                      □□▣□

        □□□□                □□□□                □□□□                □□□□

▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭  ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭

b) Update next bitmap index (node):

   33 .>. 2     = 8
    8 .&. (4-1) = 0

                                      □□▣□

        □□□□                □□□□                ▣□□□                □□□□

▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭  ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭

c) Insert value (leaf):

   33 .>. 0     = 33
   33 .&. (4-1) =  1

                                      □□▣□

        □□□□                □□□□                ▣□□□                □□□□

▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭  ▭■▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭ ▭▭▭▭
```


## References:

* Stack Overflow:
  - [Memory footprint of Haskell data types][so]
* GitLab - Glasgow Haskell Compiler:
  - [GHC Commentary: The Layout of Heap Objects][gl]
* GitLab - SPISE MISU ApS:
  - [spisemisu/arraylog][sm]

[so]: https://stackoverflow.com/a/3256825
[gl]: https://gitlab.haskell.org/ghc/ghc/wikis/commentary/rts/storage/heap-objects
[sm]: https://gitlab.com/spisemisu/arraylog
