--------------------------------------------------------------------------------
--
-- Data.Array.Log256, (c) 2020 SPISE MISU ApS
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.Array.Log256
  ( AL256
  --
  , height
  , length
  --
  , create
  , update
  , exists
  , lookup
  , remove
  --
  , pprint
  , tuples
  , tolist
  --
  , amount
  , sparse
  , defrag
  , sliver
  , expand
  , reduce
  )
where

--------------------------------------------------------------------------------

import           Prelude              hiding
  ( length
  , lookup
  )

import           Data.Bits
  ( Bits
  , shiftL
  , shiftR
  , (.&.)
  )
import           Data.List
  ( dropWhile
  , foldl'
  , takeWhile
  )
import           Data.Ratio
  ( Rational
  , (%)
  )
import           Data.Word
  ( Word8
  )

import           Data.Vector.Fixed256
  ( VF256
  )
import qualified Data.Vector.Fixed256 as VF

--------------------------------------------------------------------------------

data AL256 a
  = A !Integer !(Tree a)

data Tree a
  = L !(VF256       a)
  | N !(VF256 (Tree a))

instance Show a => Show (AL256 a) where
  show (A _ t) = show t

instance Show a => Show (Tree a) where
  show = show . helper []

--------------------------------------------------------------------------------

instance Foldable Tree where
  foldMap f (L vfa) = foldMap          f  vfa
  foldMap f (N vft) = foldMap (foldMap f) vft

instance Foldable AL256 where
  foldMap f (A _ t) = foldMap f t

instance Functor Tree where
  fmap f (L vfa) = L $ fmap       f  vfa
  fmap f (N vft) = N $ fmap (fmap f) vft

instance Functor AL256 where
  fmap f (A l t) = A l $ fmap f t

--------------------------------------------------------------------------------

length
  :: AL256 a
  -> Integer
height
  :: AL256 a
  -> Integer

-- {-# INLINE length #-}
-- {-# INLINE height #-}

length (A l _) =
  l

height (A l _) =
  log256 $ l - 1

--------------------------------------------------------------------------------

create
  :: Integer
  -> AL256 a
update
  :: Integer
  ->       a
  -> AL256 a
  -> AL256 a
exists
  :: Integer
  -> AL256 a
  -> Bool
lookup
  :: Integer
  -> AL256 a
  ->       a
remove
  :: Integer
  -> AL256 a
  -> AL256 a

-- {-# INLINE create #-}
-- {-# INLINE update #-}
-- {-# INLINE exists #-}
-- {-# INLINE lookup #-}
-- {-# INLINE remove #-}

{-| Length must be greater than 0. Devs responsability to call inside bounds
-}
create l
  | l  >  aux =                    A l $ N VF.create
  | otherwise = assert (l > 0) msg A l $ L VF.create
  where
    aux = logmod + 1
    msg = "The length of an array must be greater than 0"

{-| Length can be read in O(1). Devs responsability to call inside bounds
-}
update i a (A l t) =
  assert (i < l) msg
  A l $ aux (loglvl l) t
  where
    aux _ (L fv) =
      L $ k `seq` VF.update k a fv
      where
        k = i2wrd8 i .&. logmod
    aux j (N fv) =
      N $ v `seq` VF.update k v fv
      where
        k = i2wrd8 $ i .>. j .&. logmod
        n = j - lograt
        c =
          if   VF.exists k fv
          then VF.lookup k fv
          else branch n
        v = aux n c
    msg =
      "Index " ++ show i ++ " is out of bounds (length " ++ show l ++ ")"

{-| Length can be read in O(1). Devs responsability to call inside bounds
-}
exists i (A l t) =
  assert (i < l) msg
  aux (loglvl l) t
  where
    aux _ (L fv) =
      VF.exists k fv
      where
        k = i2wrd8 i .&. logmod
    aux j (N fv) =
      e && b
      where
        k = i2wrd8 $ i .>. j .&. logmod
        e = VF.exists k fv
        n = j - lograt
        b = aux n $ VF.lookup k fv
    msg =
      "Index " ++ show i ++ " is out of bounds (length " ++ show l ++ ")"

{-| * Length can be read in O(1). Devs responsability to call inside bounds
    * Existence can be checked in O(log₂₅₆ n). Devs responsability to check
-}
lookup i al@(A l t) =
  assert (i < l)       msg0
  assert (exists i al) msg1
  aux (loglvl l) t
  where
    aux _ (L fv) =
      VF.lookup k fv
      where
        k = i2wrd8 $ i .&. logmod
    aux j (N fv) =
      aux n c
      where
        k = i2wrd8 $ i .>. j .&. logmod
        n = j - lograt
        c = VF.lookup k fv
    msg0 =
      "Index " ++ show i ++ " is out of bounds (length " ++ show l ++ ")"
    msg1 =
      "Index " ++ show i ++ " doesn't contain any element"

{-| * Length can be read in O(1). Devs responsability to call inside bounds
    * Existence can be checked in O(log₂₅₆ n). Devs responsability to check
-}
remove i al@(A l t) =
  assert (i < l)       msg0
  assert (exists i al) msg1
  A l $ aux (loglvl l) t
  where
    aux _ (L fv) =
      L r
      where
        k = i2wrd8 $ i .&. logmod
        r = VF.remove k fv
    aux j (N fv) =
      N a
      where
        k = i2wrd8 $ i .>. j .&. logmod
        n = j - lograt
        c = VF.lookup k fv
        b = aux n c
        a =
          if      vacant b
          then VF.remove k fv
          else VF.update k b fv
    msg0 =
      "Index " ++ show i ++ " is out of bounds (length " ++ show l ++ ")"
    msg1 =
      "Index " ++ show i ++ " doesn't contain any element"

--------------------------------------------------------------------------------

pprint
  :: AL256 a
  -> String
tuples
  :: AL256 a
  -> [(Integer, a)]
tolist
  :: AL256 a
  -> [a]

-- {-# INLINE pprint #-}
-- {-# INLINE tuples #-}
-- {-# INLINE tolist #-}

pprint (A l t) =
  "├ " ++ show l ++ aux 0 t
  where
    rep i   = concat $ replicate i "│ "
    bmp f v =
      map f $
      VF.bitmap v
    aux i (L fv) =
      "\n" ++ rep i ++ "├ " ++ bmp (\x -> if x then '■' else '▭') fv
    aux i (N fv) =
      "\n" ++ rep i ++ "├ " ++ bmp (\x -> if x then '▣' else '□') fv ++ sub
      where
        sub = concat $ map (aux (i+1) . snd) $ VF.tuples fv

tuples (A _ t) =
  helper [] t

tolist (A _ t) =
  map snd $ helper [] t

--------------------------------------------------------------------------------

amount :: AL256 a -> Integer
sparse :: AL256 a -> Rational
defrag :: AL256 a -> AL256 a
sliver
  :: Integer
  -> Integer
  -> AL256 a
  -> AL256 a
expand :: Integer -> AL256 a -> AL256 a
reduce :: Integer -> AL256 a -> AL256 a

-- {-# INLINE amount #-}
-- {-# INLINE sparse #-}
-- {-# INLINE defrag #-}
-- {-# INLINE sliver #-}
-- {-# INLINE expand #-}
-- {-# INLINE reduce #-}

amount (A _ t) =
  aux t
  where
    aux (L fv) = VF.amount fv
    aux (N fv) =
      if   0 == VF.amount fv
      then 0
      else foldl' (+) 0 $ map (aux . snd) $ VF.tuples fv

sparse    (A 0 _) = 0
sparse al@(A l _) = amount al % l

defrag al =
  -- Reduces the degree of fragmentation. Check sparsity first
  foldl (\a (i,x) -> update i x a) (create n) $
  zip [ 0 .. ] $
  tolist al
  where
    n = amount al

{-| * Length can be read in O(1). Devs responsability to call inside bounds
    * Index + offset must be: i+o<=l. Devs responsability to call inside bounds
-}
sliver i o al@(A l _) =
  assert (i + o <= l) msg
  foldl (\a (j,x) -> update (j-i) x a) (create o) $
  takeWhile ((i + o >) . fst) $
  dropWhile ((i     >) . fst) $
  tuples al
  where
    msg =
      "Index i+o ("                    ++ show (i+o) ++
      ") must be less or equal to l (" ++ show  l    ++
      ")"

{-| Length can be read in O(1). Devs responsability to call inside bounds
-}
expand n (A l t) =
  assert (n > l) msg
  A n $ aux $ log256 n
  where
    msg =
      "New length " ++ show n ++ " must be greater than previous " ++ show l
    aux i
      | i  >  lvl = N $ VF.update 0 (aux $ i - 1) VF.create
      | otherwise = t
    lvl = log256 l

{-| Length can be read in O(1). Devs responsability to call inside bounds
-}
reduce n al@(A l _) =
  assert (n < l) msg
  sliver 0 n al
  where
    msg =
      "New length " ++ show n ++ " must be less than previous " ++ show l

--------------------------------------------------------------------------------

-- HELPERS

helper
  :: [Integer]
  -> Tree a
  -> [(Integer, a)]

-- {-# INLINE helper #-}

helper ns (L fv) =
  map (\(i,a) -> (fromIntegral i + (aux lograt ns), a)) $
  VF.tuples fv
  where
    aux _ [    ] = 0
    aux i (x:xs) = (1 .<. i) * x + (aux (i + lograt) xs)
helper ns (N fv) =
  concat $
  map (\(i,x) -> helper (fromIntegral i:ns) x) $
  VF.tuples fv

--------------------------------------------------------------------------------

assert
  :: Bool
  -> String
  -> a
  -> a

-- {-# INLINE assert #-}

assert True  ___ a = a
assert False msg _ = error msg

--------------------------------------------------------------------------------

branch
  :: Int
  -> Tree a
vacant
  :: Tree a
  -> Bool

-- {-# INLINE branch #-}
-- {-# INLINE vacant #-}

branch 0 = L VF.create
branch _ = N VF.create

vacant (L fv) = VF.amount fv == 0
vacant (N fv) = VF.amount fv == 0

--------------------------------------------------------------------------------

(.<.)
  :: Bits a
  =>      a
  -> Int
  ->      a
(.>.)
  :: Bits a
  =>      a
  -> Int
  ->      a

-- {-# INLINE (.<.) #-}
-- {-# INLINE (.>.) #-}

(.<.) = shiftL

(.>.) = shiftR

--------------------------------------------------------------------------------

lograt
  :: Int
logmod
  :: Num a
  =>     a
loglvl
  :: Integer
  -> Int
i2wrd8
  :: Integral a
  =>          a
  -> Word8

-- {-# INLINE lograt #-}
-- {-# INLINE logmod #-}
-- {-# INLINE loglvl #-}
-- {-# INLINE i2wrd8 #-}

lograt = 0x08

logmod = 0xFF

loglvl = ((*) lograt) . fromIntegral . log256 . (flip (-) 1)

i2wrd8 = fromIntegral

--------------------------------------------------------------------------------

-- http://graphics.stanford.edu/~seander/bithacks.html#IntegerLogObvious
--
-- log_{2^b} n ≈ m (floor)

log02b
  ::
    ( Bits a
    , Num  a
    )
  => Int
  ->       a
  ->       a
log256
  ::
    ( Bits a
    , Num  a
    )
  =>       a
  ->       a

-- {-# INLINE log02b #-}
-- {-# INLINE log256 #-}

log02b b n =
  aux 0 (n .>. b)
  where
    aux acc 0 = acc
    aux acc x = aux (acc + 1) (x .>. b)

log256 =
  log02b lograt
