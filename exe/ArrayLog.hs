--------------------------------------------------------------------------------
--
-- ArrayLog, (c) 2020 SPISE MISU ApS
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Prelude           hiding
  ( length
  , lookup
  )

import           Data.Array.Log256

--------------------------------------------------------------------------------

main :: IO ()
main =
  (putStrLn "# Before fmap")     >>
  (putStrLn . show . tuples $ a) >>
  (putStrLn . pprint        $ a) >>
  (putStrLn "")                  >>

  (putStrLn "# After fmap")      >>
  (putStrLn . show . tuples $ b) >>
  (putStrLn . pprint        $ b) >>
  (putStrLn "")                  >>

  (putStrLn "# Sparse (create)") >>
  (putStrLn . show   $ sparse a) >>
  (putStrLn "")                  >>

  (putStrLn "# Sparse (sliver)") >>
  (putStrLn . show   $ sparse s) >>
  (putStrLn . pprint        $ s) >>
  (putStrLn "")                  >>

  (putStrLn "# Sparse (expand)") >>
  (putStrLn . show   $ sparse e) >>
  (putStrLn . pprint        $ e) >>
  (putStrLn "")                  >>

  (putStrLn "# Sparse (defrag)") >>
  (putStrLn . show   $ sparse d) >>
  (putStrLn . pprint        $ d)
  where
    n = 300
    m = 150
    --
    a = foldl (\ acc x -> update x x acc) (create n) $ take m [ 0 .. ]
    b = fmap (*2) a
    --
    i = 050
    o = 100
    s = sliver i o b
    --
    e = expand 65536 s
    --
    d = defrag e
