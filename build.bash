#!/usr/bin/env bash

clear

bld="$(pwd)/.stack-work/install/x86_64-linux-nix/"
tgt="$(pwd)/bin"

echo "### Ensure folders exist:"
mkdir -v -p $bld;
mkdir -v -p $tgt;
echo

echo "### Clearing binaries:"
find $bld -mindepth 1 -name "*" -delete -print
find $tgt -mindepth 1 -name "*" -delete -print
echo

echo "### Stack building:" 
#stack clean
#stack build --verbosity debug
#stack build --fast
stack build
echo

src="$(stack path --local-install-root)/bin"

echo "### Copying binaries to local $tgt:" 
cp -v $src/* $tgt/
echo

echo "### Repoducible hashes:"
cd $tgt
for f in *; do
    # skip all non-binaries
    [[ $f == *.* ]] && continue
    echo -e $(sha256sum $f | cut -d " " -f 1): $f
done;
echo
